const PROGMEM float turn_on_voltage = 13.4; // input voltage over this level signals battery charging
const PROGMEM float turn_off_voltage=12.4; // input voltage below this level signals battery low
const PROGMEM int low_voltage_seconds = 60; //input voltage must be below threshold for this time to trigger low voltage shutoff
const PROGMEM int poweroff_delay_hours=1; //poweroff delay
const PROGMEM int poweroff_delay_minutes=0;
const PROGMEM int poweroff_delay_seconds=0; 
const PROGMEM long int poweroff_delay_total = (poweroff_delay_seconds + 60*(poweroff_delay_minutes + 60 * poweroff_delay_hours));

const PROGMEM unsigned long int resistor_divider_top = 47000;
const PROGMEM unsigned long int resistor_divider_bottom = 15000;
const PROGMEM float resistor_scale = (resistor_divider_top + resistor_divider_bottom) / ((float)resistor_divider_bottom) ;

const PROGMEM float analog_v_reference = 4.3;
const PROGMEM int ADC_counts_max = 1023;
const PROGMEM float ADC_scale = analog_v_reference / ADC_counts_max;
const PROGMEM float correction_factor = 1.0135;

const PROGMEM int power_pin = 4;
const PROGMEM int battery_voltage_pin = A4;

const PROGMEM int drive_cycle_seconds = 2;

void open_drain_low(int pin){
  digitalWrite(pin, LOW);
  pinMode(pin, OUTPUT);
}

void open_drain_high(int pin){
  pinMode(pin, INPUT);
}

float Battery_voltage(){
  int counts = analogRead(battery_voltage_pin);
  //Serial.print("battery counts");

//  Serial.print("\nbattery voltage counts is ");
//  Serial.print(counts);
  float rawVolts = counts * ADC_scale; 
  float Volts = rawVolts * resistor_scale;
//  Serial.print(counts);
//  Serial.print(" ");
//  Serial.print(rawVolts);
//  Serial.print(" ");
//  Serial.print(resistor_scale);
//  Serial.print(" ");
//  Serial.println(Volts);
  return Volts * correction_factor;
}
String Format_seconds(unsigned long seconds){
  unsigned long tme = seconds;                                                           //Time we are converting. This can be passed from another function.
  unsigned long hr = tme/3600;                                                        //Number of seconds in an hour
  unsigned long mins = (tme-hr*3600)/60;                                              //Remove the number of hours and calculate the minutes.
  unsigned long sec = tme-hr*3600-mins*60;                                            //Remove the number of hours and minutes, leaving only seconds.
  String hrMinSec = (String(hr) + ":" + String(mins) + ":" + String(sec));  //Converts to HH:MM:SS string. This can be returned to the calling function.
  return hrMinSec;
}

int getTemperature(void) {
VREF.CTRLA = VREF_ADC0REFSEL_1V1_gc; // 1.1 volt reference
ADC0.CTRLC = ADC_SAMPCAP_bm + 3; // VREF reference, correct clock divisor
ADC0.MUXPOS = ADC_MUXPOS_TEMPSENSE_gc; // Select temperature sensor
ADC0.CTRLD = 2 * (1 << ADC_INITDLY_gp); // Initial delay of 32us 
ADC0.SAMPCTRL = 31; // Maximum length sample time (32us is desired) 
ADC0.COMMAND = 1; // Start the conversion 
while ((ADC0.INTFLAGS & ADC_RESRDY_bm) == 0) ; // wait for completion 
// The following code is based on the ATmega4809 data sheet
int8_t sigrow_offset = SIGROW.TEMPSENSE1; // Read signed value from signature row 
uint8_t sigrow_gain = SIGROW.TEMPSENSE0; // Read unsigned value from signature row 
uint16_t adc_reading = ADC0.RES; // ADC conversion result with 1.1 V internal reference 
uint32_t temp = adc_reading - sigrow_offset; 
temp *= sigrow_gain; // Result might overflow 16 bit variable (10bit+8bit) 
temp += 0x80; // Add 1/2 to get correct rounding on division below 
temp >>= 8; // Divide result to get Kelvin
uint16_t temperature_in_K = temp;
ADC0.CTRLD = ADC0.SAMPCTRL=0;
analogReference(INTERNAL4V3);
return temperature_in_K - 273; // Return Celsius temperature
}
unsigned long getTime(void){
    

}
