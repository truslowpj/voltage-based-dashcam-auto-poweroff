#include <RocketScream_RTCAVRZero.h>
#include <RocketScream_LowPowerAVRZero.h>

#include "dashcam.h"
const uint8_t unusedPins[] = { 2, 3, 5, 6, 7, 8, 9, 10, 11, 12,
                              14, 15, 16, 17, 19, 20, 21};
bool low_voltage_triggered = false;
bool poweroff_delay_triggered = false;
bool driving = false;
unsigned long last_charging = 0;
unsigned long last_above_low = 0;
unsigned long last_driving = 0;
#define LOG_LENGTH 500

uint16_t drive_time[LOG_LENGTH];
uint16_t min_voltage_counts[LOG_LENGTH];
uint32_t time_since_last_drive[LOG_LENGTH];

unsigned long drive_start = 0;
unsigned long drive_stop = 0;
uint16_t min_voltage = 65000;
uint16_t drive_index = 0;
unsigned long pre_log_seconds = 0;

float low_pass_voltage = 14;

unsigned long last_printout = 0;
unsigned long last_run = 0;
unsigned long now_raw = 0;
unsigned long now = 0;
bool logging_enabled = false;
unsigned long serial_last_received = 0;
void setup() {
  // put your setup code here, to run once:
  analogReference(INTERNAL4V3);
  open_drain_low(power_pin);
  Serial.begin(115200);

  uint8_t index;

  for (index = 0; index < sizeof(unusedPins); index++)
  {
    pinMode(unusedPins[index], INPUT_PULLUP);
    LowPower.disablePinISC(unusedPins[index]);
  }

  RTCAVRZero.begin(false);
  RTCAVRZero.enableAlarm(1, true);
  RTCAVRZero.attachInterrupt(update_funct);
  
}

void loop() {
  Serial.flush();
  digitalWrite(1, HIGH);
  LowPower.standby();

  now = now_raw * ((double)1200)/1213.0;
float battery_voltage = Battery_voltage();
  battery_voltage = Battery_voltage();
  battery_voltage = Battery_voltage();
  battery_voltage = Battery_voltage();
  unsigned long now_millis = millis();

  low_pass_voltage = 0.9 * low_pass_voltage + 0.1 * battery_voltage;
  
  last_run = now_millis;
  

  
  if (battery_voltage > turn_on_voltage){
    last_charging = now;
    if (!driving){
      driving = true;
      drive_start = now;
      time_since_last_drive[drive_index] = drive_start - drive_stop;
      min_voltage_counts[drive_index] = min_voltage;
      min_voltage = 65000;
      Serial.print(F("started driving at "));
      Serial.println(now);

      /////////////////logging printout section
      if (true){
        while(Serial.available() > 0) Serial.read();
        int log_start, log_end;
        serial_last_received = now;
        if (pre_log_seconds == 0){
          log_start = 0;
          log_end = drive_index;
        } else log_start = drive_index + 1;
        unsigned long log_time = pre_log_seconds;
        for (int i=log_start; i<log_end; i++){

          Serial.print(Format_seconds(log_time));
          Serial.print(F(": time between drives : "));
          Serial.print(Format_seconds(time_since_last_drive[i]));
          Serial.print(F(", lowest voltage since last drive: "));
          Serial.println(min_voltage_counts[i] / 1000.0);
          log_time += time_since_last_drive[i];
          Serial.print(Format_seconds(log_time));
          Serial.print(F(": drive started, drive time : "));
          Serial.println(Format_seconds(drive_time[i]));
          log_time += drive_time[i];
        }
      }

      
    }
    poweroff_delay_triggered = false;
    low_voltage_triggered = false;
  }
  
  if (battery_voltage > turn_off_voltage){
    last_above_low = now;
  }

  if ((now - last_charging) > drive_cycle_seconds){
    if (driving){
      driving = false;
      drive_stop = now;
      drive_time[drive_index] = drive_stop - drive_start;
      drive_index++;
      Serial.print(F("stopped driving at "));
      Serial.println(now);
    }
  }

if (driving){
  last_driving = now;
}

if (!driving){

  if ((low_pass_voltage * 1000) < min_voltage){
    min_voltage = low_pass_voltage * 1000;
  }
  
    if ((now - last_above_low) > low_voltage_seconds){
      if (!low_voltage_triggered){
        low_voltage_triggered = true;
        Serial.print(F("low voltage protection triggered at "));
        Serial.print(battery_voltage);
        Serial.print(F(" volts at "));
        Serial.println(now);
      }    
  }

    if ((now - last_driving) > poweroff_delay_total){
      if (!poweroff_delay_triggered){
            poweroff_delay_triggered = true;
            Serial.print(F("poweroff delay triggered at "));
            Serial.println(now);
      }

    }


  
}

pinMode(13, OUTPUT);
  if (low_voltage_triggered || poweroff_delay_triggered){
    digitalWrite(13, false);
    open_drain_low(power_pin);
  }
  else digitalWrite(13, true),open_drain_high(power_pin);

  if (true){
    last_printout = millis();
    Serial.print(F("battery voltage is "));
    Serial.print(battery_voltage);
    Serial.print(F(" low pass battery voltage is "));
    Serial.print(low_pass_voltage);
    Serial.print(F(" Temperature "));
    Serial.print(getTemperature());
    Serial.print(F(" drive index is "));
    Serial.print(drive_index);
    Serial.print(F(" time is "));
    Serial.println(now);
  }



}



void update_funct(){
  
  now_raw++;
  
}
